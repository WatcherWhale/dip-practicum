%im = im2gray(imread("liver.jpg"));
im = im2gray(imread("liver_cyste.jpg"));
imshow(im)
im = imgaussfilt(im,2);
imshow(im)


rb = 60 %remove black
rw = 160 %remove white

mask=((im >= rb) & (im < rw));
imshow(mask) %we kijken alleen naar dit deel van de foto

im = im.* uint8(mask);
imshow(im)
im = im + uint8(1-mask)*255;
imshow(im) %al het zwart en wit is nu weg

[counts,x] = imhist(im, 255);

counts = counts(rb:rw);
x = x(rb:rw);
stem(x, counts);


mean_gray = uint8(mean(x))

index = find(counts == max(counts))

max_gray = uint8(x(index))

im = im < max_gray + 10 & im > max_gray - 10;
imshow(im) %donkere stukken zoeken

eroded = imerode(im,strel('disk',5))
imshow(eroded)
dilated = imdilate(eroded, strel('disk',10))
imshow(dilated)
