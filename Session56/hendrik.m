%cyst
imCyst=imgaussfilt(imread("liver_cyste.jpg"),2);

T = graythresh(imCyst) * 255

binCyst=((imCyst >= 80) & (imCyst < 90));
edgcyst=(edge(imerode(binCyst,strel('square', 5)),'canny'));

whitcyst=(imdilate(edgcyst,strel('sphere',20)));

[b,l] = size(whitcyst);
A = cat(3, uint8(whitcyst*255), zeros(b,l), zeros(b,l));
mask = cat(3, 1 - uint8(whitcyst*255), 1 - uint8(whitcyst*255), 1 - uint8(whitcyst*255));

imshow(A + imCyst)

