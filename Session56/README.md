# Detecting abnormalities on an image

## 0. Blurring the image

Our images contain a lot of noise, removing this by a gaussian blur becomes quite handy.

## 1. Selecting the liver

1. First decrease the brightness of the image.

2. Remove the overly white features from the image.

3. Do edge detection (canny)

4. fill in the selected edges

5. Select only the biggest area (this is the liver)

## 2. Tresholding

1. Apply average tresholding iteratively

This treshold doesn't include black values.

2. Do this n times

## 3. Finding cystes

1. We erode the image with a given size

--> This way we only find the abnormalities

2. We dialate the eroded picture to give a selection of the whereabouts of the abnormalities

3. Optionally we highlight these abnormalities on the image
