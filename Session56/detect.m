function [detected, highlighted] = detect(im, it, gamm, dSize, color)

    if(~exist('it', 'var'))
        it = 3;
    end
    if(~exist('gamm', 'var'))
        gamm = 5.2;
    end
    if(~exist('dSize', 'var'))
        dSize = 8;
    end
    if(~exist('color', 'var'))
        color = [0,0,1];
    end

    im = im2gray(im);
    im_blur = imgaussfilt(im, 2);

    ia = imadjust(im_blur, [], [], gamm);
    ia = ia .* uint8( 1 - imdilate(imbinarize(ia, 0.5), strel('sphere', 5)));
    ed = edge(imbinarize(ia), 'canny');

    liver = bwconvhull(ed, 'objects');
    liver = biggestArea(liver);

    T = graythresh( uint8(liver) .* im_blur);
    binIm = imbinarize( uint8(liver) .* im_blur , T);
    cleared_Im = (im_blur) .* uint8(liver);

    for n = 1:it
        cleared_Im = uint8(uint8(binIm) .* cleared_Im);
        T = thresh(cleared_Im);

        binIm = (1-imbinarize(cleared_Im, T) ) .* binIm;
    end

    eroded = imerode(binIm,strel('square', dSize));
    detected = imdilate(eroded, strel('sphere', 20));

    [b, l] = size(detected);
    highlight = cat(3, uint8(detected*255) * color(1), uint8(detected*255) * color(2), uint8(detected*255) * color(3));

    highlighted = highlight + im;
end


function [avg] = thresh(im)
    avg = sum(sum(im)) ./ (sum(sum((im > 0))) * 255);
end

function [BW_out] = biggestArea(BW)

    pieces = regionprops(BW, 'Area', 'PixelList');

    ar = 0;
    pixellist = [];

    for p = 1:size(pieces)
        piece = pieces(p);

        if piece.Area > ar
            ar = piece.Area;
            pixellist = piece.PixelList;
        end
    end

    [b,l] = size(BW);
    BW_out = zeros(b, l);

    for i = 1:size(pixellist(:,1))
        BW_out(pixellist(i,2), pixellist(i,1)) = 1;
    end

end
