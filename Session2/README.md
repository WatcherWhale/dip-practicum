# Session 2: Notes

## Exercise 1

Files: `O1.m`, `O1bigger.m`, `add_frame.m`, `add_frame_for.m`

Example timing outputs
```
>> O1
Elapsed time is 0.002841 seconds.
Elapsed time is 0.002385 seconds.
>> O1
Elapsed time is 0.000748 seconds.
Elapsed time is 0.003698 seconds.
>> O1
Elapsed time is 0.001467 seconds.
Elapsed time is 0.002615 seconds.
>> O1
Elapsed time is 0.002293 seconds.
Elapsed time is 0.001289 seconds.
>> O1
Elapsed time is 0.001126 seconds.
Elapsed time is 0.001831 seconds.
>> O1
Elapsed time is 0.000622 seconds.
Elapsed time is 0.001507 seconds.
>> O1
Elapsed time is 0.000481 seconds.
Elapsed time is 0.001175 seconds.
>> O1
Elapsed time is 0.000304 seconds.
Elapsed time is 0.000697 seconds.
>> O1
Elapsed time is 0.000497 seconds.
Elapsed time is 0.001788 seconds.
>> O1
Elapsed time is 0.000239 seconds.
Elapsed time is 0.000704 seconds.
>> O1
Elapsed time is 0.000363 seconds.
Elapsed time is 0.001136 seconds.
>> O1
Elapsed time is 0.000834 seconds.
Elapsed time is 0.003220 seconds.
```

**Notice:**

You'll notice that the timings are all over the place. Sometimes the for loop takes a shorter amount of time than the non for loop function. this is due the inaccuracy of the timing with smaal timeframes.

We can solve this by executing the functions multiple times

Example with 10 000 loops
```
>> O1
Elapsed time is 0.965569 seconds.
Elapsed time is 3.016224 seconds.
>> O1
Elapsed time is 0.984993 seconds.
Elapsed time is 2.932794 seconds.
>> O1
Elapsed time is 1.132768 seconds.
Elapsed time is 3.008860 seconds.
>> O1
Elapsed time is 1.188159 seconds.
Elapsed time is 3.163219 seconds.
```

## Exercise 2

### Article 1

[https://nl.mathworks.com/company/newsletters/articles/matrix-indexing-in-matlab.html](https://nl.mathworks.com/company/newsletters/articles/matrix-indexing-in-matlab.html)

### Article 2

[https://blogs.mathworks.com/loren/2019/05/29/big-data-in-mat-files/](https://blogs.mathworks.com/loren/2019/05/29/big-data-in-mat-files/)

