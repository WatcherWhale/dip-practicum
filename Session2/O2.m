%% Article 1

IM = imread('baboon.jpg');
IM = rgb2gray(IM);

[r, c] = size(IM);
center = [r/2, c/2];

IM1 = IM([r:-2:center(1) center(1):-2:1], ...
    [c:-1:center(2) center(2):-1:1]);

imshow(IM1)

%% Article 2

Baboon = IM;
BaboonStreched = IM1;

save baboon.mat Baboon BaboonStreched;

clear;

load baboon.mat;

imshow(BaboonStreched);
