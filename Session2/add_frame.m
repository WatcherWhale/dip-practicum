function [im_out] = add_frame(im_in) 
    % adds a frame to an existing grayscale image 
    % im_in: original grayscale image 
    % im_out: black frame of 5 wide is added to im_in 
    [r,c]=size(im_in);

    % White frame
    %im_out=uint8(255*ones(r+10,c+10)); 
    % Black Frame
    im_out=uint8(255*ones(r+10,c+10)); 

    im_out(6:r+5,6:c+5)=im_in; 
end
