function [im_out] = add_frame_for(im_in) 
    % adds a frame to an existing grayscale image 
    % im_in: original grayscale image 
    % im_out: black frame of 5 wide is added to im_in 
    [r,c]=size(im_in); 

    im_out = uint8(zeros(r+10, c+10));

    for i = 6:r
        for j = 6:c
            im_out(i,j) = im_in(i - 5,j - 5);
        end
    end
end
