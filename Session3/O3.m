spectrum = imread('spectrum.tif');
spectrum = double(spectrum)/255;

g = @(f, m, E) (1./(1 + (m ./ double(f) + eps)) .^E) / 255;

spectrum2 = g(spectrum, 0.3, 2);
imshowpair(spectrum,spectrum2, 'montage')

%figure;

% E groter => Donkerder
% E kleiner => lichter
spectrum3 = g(spectrum, 0.5, 1);
%imshowpair(spectrum,spectrum3,'montage')

figure

% m keep bright elemenrs or not
spectrum3 = g(spectrum, 5, 2);
imshowpair(spectrum,spectrum3,'montage')
