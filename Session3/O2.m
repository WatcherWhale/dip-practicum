% Oppervlakte Threshold

Ta = 255;

rice = imread('rice.gif');

T = graythresh(rice);
mask = im2bw(rice, T);

pieces = regionprops(mask, 'Area', 'PixelList');

for p = 1:size(pieces)
    piece = pieces(p);

    if piece.Area < Ta
        pixels = piece.PixelList;
        for i = 1:size(pixels(:,1))
            mask(pixels(i,2),pixels(i,1)) = 0;
        end
    end
end

riceDecluttered = uint8(mask) .* rice;

[L, n] = bwlabel(mask);

disp("There are " + n + " pieces of rice left.")

imshowpair(rice, riceDecluttered,'montage')
