c = imread('trees.tif');

imshow(c);


cd = double(c);
%figure;
%imshow(cd);

c0 = mod(cd, 2);
%figure;
%imshow(c0);

c1 = mod(floor(cd / 2), 2);
%figure;
%imshow(c1);

c2 = mod(floor(cd / 4), 2);
%figure;
%imshow(c2);

c3 = mod(floor(cd / 8), 2);
%figure;
%imshow(c3);

c4 = mod(floor(cd / 16), 2);
%figure;
%imshow(c4);

c5 = mod(floor(cd / 32), 2);
%figure;
%imshow(c5);

c6 = mod(floor(cd / 64), 2);
%figure;
%imshow(c6);

c7 = mod(floor(cd / 128), 2);
%figure;
%imshow(c7);

figure;
imshow( uint8(c0 + c1 * 2 + c2 * 4 + c3 * 8 + c4 * 4 + c5 * 32 + c6 * 64 + c7 * 128) );
