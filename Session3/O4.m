pollen = imread('pollen.tif');
bones = imread('bone-scan.tif');

g = @(f, m, E) (1./(1 + (m ./ double(f) + eps)) .^E) / 255;
h = @(f, c)  c * log(1+double(f)) / 255;

imshowpair(pollen,bones, 'montage')
figure;
imshowpair(g(pollen, 255/2, 3), h(pollen, 1) ,'montage')
figure;
imshowpair(g(bones, 255/2, 3), h(bones, 1) ,'montage')
