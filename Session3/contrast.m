f=imread('chest-xray.tif');
imshow(f);
%g1=imadjust(f);
%imshow(g1);

% Een soort van geinverteerde image
%g2=imadjust(f,[0,1],[1,0]);
%figure,imshow(g2);

% map the brightness value between 0 -> 0.2 to 0 -> 1 (more brightness)
%g3=imadjust(f,[0,0.2],[0,1]);
%figure,imshow(g3);

% Lower the gamma so that our graph goes faster to high_out
g4=imadjust(f,[],[],0.2);
figure, imshow(g4);
