clear;
close all;
im=imread('rice.gif');
imshow(im);
figure;
imhist(im);
im=mat2gray(im); % transform to set range to [0,1]
figure;
imshow(im);
figure;
imhist(im);
im=imread('rice.gif');
T=graythresh(im); % matlab calculates the threshold value T (check matlab help) 
im=im2bw(im,T); % transforms into black white image according to threshold T 
figure;
imshow(im);
figure;
imhist(im);

