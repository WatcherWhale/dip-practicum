IM1 = imread('moss.JPG');
IM2 = imread('squirrel.jpg');

%IM1 = rgb2gray(IM1);
%IM2 = rgb2gray(IM2);

masker = uint8(roipoly(IM2));

IM3 = IM1 .* (1-masker) + IM2 .* masker;

imshow(IM3);
