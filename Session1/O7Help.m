%% Exercise 6
% met hebulp va,nl.mathworks.com/help/images/ref/imfindcircles.html
IM = imread('moon.tif');
imshow(IM);
[centers,radii] = imfindcircles(IM,[5 50],'ObjectPolarity', 'dark', 'Sensitivity',0.9);
imshow(IM)
h = viscircles(centers,radii);
