moon =  imread('moon.tif');
windowSize = 1;
avg3 = ones(windowSize) / windowSize^2;
moon = (imfilter(I, avg3, 'conv'));
moonb = 1 - imbinarize(moon,'adaptive','ForegroundPolarity','dark', 'Sensitivity', 0.5);
imshow(moonb)
figure;

stats = regionprops('table', moonb,'Centroid', 'MajorAxisLength','MinorAxisLength');

centers = stats.Centroid;
diameters = mean([stats.MajorAxisLength stats.MinorAxisLength],2);
radii = diameters/2;

imshow(moon)
hold on
viscircles(centers,radii);
hold off