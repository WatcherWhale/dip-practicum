IM1 = imread('baboon.jpg');
IM2 = imread('jail.jpg');

S = size(IM1);
IM2 = imresize(IM2, S(1:2));

UL = rgb2gray(IM1);
UR = rgb2gray(IM2);

BL = UL .* 2;
BR = UR./2 + UL;

subplot(2, 2, 1);
subimage(UL);
subplot(2, 2, 2);
subimage(UR);
subplot(2, 2, 3);
subimage(BL);
subplot(2, 2, 4);
subimage(BR);

% The last one is bright because you add a full image on top of an half one meaning you can get 1.5 times the color
% If both images were halved the over brightness cannot happen
