M = randi([0, 255], [100, 100]);
N = M >= 100;

imshowpair(M, N, 'montage')

% imbinarize: command to make a binary image out of an image
% imbinarize(<image>, <threshold>)
O = imbinarize(M,100);

imshowpair(M, O, 'montage')

